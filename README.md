# Knownion Misc

* `xx.py` (OLD) a draft to send data in python to the server with a correct format 
* `zip.py` make a zip file with image and doc 
* `put_send.sh` (NEW) send a PUT request with curl 
* `misspelling.py` some search engine reflexion (and beginning implementation) 
* `knownion_doc.zip` typical zip to send to the server (contain one markdown and two images)