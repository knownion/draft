<!--
{
	"keywords": [
		"docker",
		"bug solving",
		"docker 19",
		"Dockerfile",
		"copy folder in container"
	]
}
-->
# Use sudoers file

There is a file `/etc/sudoers` that you can modify, to grant root access to a user without the password need.

## Example:

In this example we give the user `olivier` acces to the application `vmhgfs` located at `/usr/bin/vmhgfs`

```
olivier ALL(ALL) NOPASSWD: /usr/bin/vmhgfs
```

## Explications based on above example

if we translate the code above in sentences we have:

* `olivier` is the username if you want a group use `%group` with the '%'
* `ALL(ALL)`
  * the first `ALL` represent the hosts this rule apply to
  * the second `ALL` represent from which user we take privileges
* `NOPASSWD:` after this all following commands don't ask password (moreover we still need to use `sudo`)
* `/usr/bin/vmhgfs` it corresponds to program names in absolute path.


## Few more examples

To deny access to an application for an user we use `!` before the command

```
user ALL(ALL) !/absolute/path/to/command
```

To allow a group to access a ressource without password use `%` before the group name

```
%group ALL(ALL) NOPASSWD: /absolute/path/to/command
```
