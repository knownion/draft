<!--
{
	"keywords": [
		"docker",
		"bug solving",
		"docker 19",
		"Dockerfile",
		"copy folder in container"
	]
}
-->
# Copy a folder using a Dockerfile

the folling command will copy the folder `src` into `/app/` <br/>
there is a `src` folder and all its content in the container 

```
COPY src /app/src
```

the following command will copy the contents of `src` into `/app/` <br/>
result: there is no `src` folder in the container, only the contain of `src`
```
COPY src /app
```

