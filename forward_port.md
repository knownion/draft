<!--
{
	"keywords": [
		"ip route",
		"ip table",
		"0.0.0.0",
		"open a port from external network",
		"listen external trafic on linux",
		"route external trafic on linux",
		"serveur respond to external request on linux",
		"accept external connection",
		"accept remote connection",
		"network",
		"port",
		"linux",
		"netstat",
		"linux network"
	]
}
-->


# Forward ports to accept remote connections on server

## Context
* In my case I have a Flask server listening on `0.0.0.0:5000`. <br/>
* I'm able to connect to the server from my machine but **connections are refusing** when it come from **remote clients**. <br/>
* My machines are on the same network.

## Forward port
I look if my server is listening on the correct ip and port 

```sh
netstat -tupln | grep ':5000'
```

Then I use IPTable to accept external request to 5000 to my internal server listening on port 5000 

```sh
iptables -I INPUT -p tcp --dport 5000 -j ACCEPT
```